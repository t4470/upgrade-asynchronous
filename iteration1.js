// Iteración #1: Fetch

/* 1.1 Utiliza esta url de la api Agify 'https://api.agify.io?name=michael' para
hacer un .fetch() y recibir los datos que devuelve. Imprimelo mediante un
console.log(). Para ello, es necesario que crees un .html y un .js. */
fetch('https://api.agify.io?name=michael')
    .then((response) => {
        return response.json();
    })
    .then((myJson) => {
        console.log(myJson);
    });


/* 2.1 Dado el siguiente javascript y html. Añade la funcionalidad necesaria usando
fetch() para hacer una consulta a la api cuando se haga click en el botón,
pasando como parametro de la api, el valor del input. */
var body = document.querySelector('body');

const $$button = document.querySelector('button');
$$button.id = 'button-api';
$$button.addEventListener('click', clickButton);


function clickButton () {
    const $$value = document.querySelector('input').value;
    const baseUrl = `https://api.nationalize.io?name=${$$value}`;

    fetch(baseUrl)
    .then((response) => {
        
        return response.json();
    })
    .then((myJson) => {
        console.log(myJson);
        var probability = myJson.country[0].probability;
        probability = probability.toFixed(2);
        var probability2 = myJson.country[1].probability;
        probability2 = probability2.toFixed(4);
        elem.textContent = `El nombre ${$$value} tiene un ${probability} porciento de ser de ${myJson.country[0].country_id}
                            y un  ${probability2} de ser de ${myJson.country[1].country_id}`
    })
    
    var elem = document.createElement('p');
    elem.className = 'container';
    elem.style = 'margin-top: 1em; padding: 1em; border: 1px solid red; height: 30px; display: flex;';
    body.appendChild(elem);

    const $$button2 = document.createElement('button');
    $$button2.className = 'button-remove';
    $$button2.textContent = 'X';
    $$button2.style = 'display:block; margin-top: 1em';
    var parentDiv = document.querySelector(".container").parentNode;
    parentDiv.insertBefore($$button2, elem);
    $$button2.addEventListener('click', deleteButton);
}

/* 2.3 En base al ejercicio anterior. Crea dinamicamente un elemento  por cada petición
a la api que diga...'El nombre X tiene un Y porciento de ser de Z' etc etc.
EJ: El nombre Pepe tiene un 22 porciento de ser de ET y un 6 porciento de ser
de MZ. */

// HECHO ARRIBA

/* 2.4 En base al ejercicio anterior, crea un botón con el texto 'X' para cada uno
de los p que hayas insertado y que si el usuario hace click en este botón
eliminemos el parrafo asociado. */
    function deleteButton(e) {
        // $$remove = document.querySelector(".button-remove");
        e.currentTarget.style = 'display: none;';
        e.currentTarget.nextSibling.remove();
    };






